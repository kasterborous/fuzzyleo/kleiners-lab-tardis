-- Adds default rails

local PART={}
PART.ID = "kleiner_base2"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/Kleiner/base2.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_base3"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/Kleiner/base3.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_base3classic"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/Kleiner/base3classic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_book1"
PART.Name = "Rails"
PART.Model = "models/props_lab/binderblue.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_book2"
PART.Name = "book"
PART.Model = "models/props_lab/binderredlabel.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_book3"
PART.Name = "book3"
PART.Model = "models/props_lab/bindergraylabel01b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_book4"
PART.Name = "book4"
PART.Model = "models/props_lab/bindergreen.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_book5"
PART.Name = "book5"
PART.Model = "models/props_lab/bindergreen.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_book6"
PART.Name = "book6"
PART.Model = "models/props_lab/binderbluelabel.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_boxkeypostwhatever"
PART.Name = "boxkeypostwhatever"
PART.Model = "models/props_lab/partsbin01.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_boxlamp1"
PART.Name = "boxlamp1"
PART.Model = "models/props_c17/light_decklight01_on.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_boxlamp2"
PART.Name = "boxlamp2"
PART.Model = "models/props_c17/light_decklight01_on.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_bucket"
PART.Name = "bucket"
PART.Model = "models/props_junk/plasticbucket001a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_can"
PART.Name = "can"
PART.Model = "models/props_junk/PopCan01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_canister"
PART.Name = "canister"
PART.Model = "models/props_c17/canister01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_cardboardbox1"
PART.Name = "cardboardbox1"
PART.Model = "models/props_junk/cardboard_box003a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_cardboardbox2"
PART.Name = "cardboardbox2"
PART.Model = "models/props_junk/cardboard_box001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_cardboardbox3"
PART.Name = "cardboard box"
PART.Model = "models/props_junk/cardboard_box001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_cardboardbox4"
PART.Name = "cardboard box"
PART.Model = "models/props_junk/cardboard_box001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_cardboardbox5"
PART.Name = "cardboard box"
PART.Model = "models/props_junk/cardboard_box001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_cloak"
PART.Name = "kleiner_cloak"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_computerbank"
PART.Name = "bank"
PART.Model = "models/props_lab/servers.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_computerbank2"
PART.Name = "bank"
PART.Model = "models/props_lab/servers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_computerbank3"
PART.Name = "bank3"
PART.Model = "models/props_lab/workspace003.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_consolebox"
PART.Name = "consolebox"
PART.Model = "models/props_c17/consolebox05a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_console1"
PART.Name = "console"
PART.Model = "models/props_lab/workspace001.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_consolekeyboard"
PART.Name = "tankconsole"
PART.Model = "models/props_c17/computer01_keyboard.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_consoleteleport"
PART.Name = "consoleteleport"
PART.Model = "models/props_lab/workspace002.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_coordshitbox"
PART.Name = "coordinates"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate1"
PART.Name = "crate"
PART.Model = "models/props_junk/PlasticCrate01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate2"
PART.Name = "crate2"
PART.Model = "models/props_junk/wood_crate002a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate3"
PART.Name = "crate3"
PART.Model = "models/props_junk/wood_crate002a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate4"
PART.Name = "crate4"
PART.Model = "models/props_junk/wood_crate002a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate5"
PART.Name = "crate5"
PART.Model = "models/props_junk/wood_crate002a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate6"
PART.Name = "crate6"
PART.Model = "models/props_junk/PlasticCrate01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_crate7"
PART.Name = "crate7"
PART.Model = "models/props_junk/PlasticCrate01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_demathitbox"
PART.Name = "throttle"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "ambient/machines/keyboard_fast3_1second.wav"

if SERVER then
    function PART:Use(ply)
        if self.Control ~= "teleport_double" then
            TARDIS:Control(self.Control, ply, self)
        end

        if self.exterior:GetData("teleport") == true or self.exterior:GetData("vortex") == true
            or not self.interior:GetSequencesEnabled()
        then
            TARDIS:Control("teleport_double", ply, self)
        else
            TARDIS:ErrorMessage(ply, "Common.ControlSequencesEnabledWarning")
        end
    end
end

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_doorcontrol2"
PART.Name = "doorcontrol2"
PART.Model = "models/props_lab/powerbox02b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_doorframe"
PART.Name = "doorframe"
PART.Model = "models/FuzzyLeo/Kleiner/torrentsoda.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_doorlockhitbox"
PART.Name = "doorlockhitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_doortogglehitbox"
PART.Name = "doortogglehitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_doorvoid"
PART.Name = "doorvoid"
PART.Model = "models/FuzzyLeo/Kleiner/doorvoid.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_enginereleasehitbox"
PART.Name = "enginereleasehitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_eyescanner"
PART.Name = "kleiner_eyescanner"
PART.Model = "models/props_lab/eyescanner.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_fastreturn"
PART.Name = "kleiner_fastreturn"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_fence1"
PART.Name = "fence1"
PART.Model = "models/props_c17/fence03a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_fence2"
PART.Name = "fence2"
PART.Model = "models/props_c17/fence01b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_fence3"
PART.Name = "fence3"
PART.Model = "models/props_c17/fence01b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_filecabinet"
PART.Name = "filecabinet"
PART.Model = "models/props_lab/filecabinet02.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_flighthitbox"
PART.Name = "Flight"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_float"
PART.Name = "kleiner_float"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_hadshitbox"
PART.Name = "kleiner_hadshitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_handbrake"
PART.Name = "kleiner_handbrake"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_hevcase"
PART.Name = "hevcase"
PART.Model = "models/props_lab/HEV_case.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_hevmachine"
PART.Name = "hevmachine"
PART.Model = "models/props_lab/hev_machine.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_hose"
PART.Name = "hose"
PART.Model = "models/props_lab/generatorhose.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_hula"
PART.Name = "hula"
PART.Model = "models/props_lab/huladoll.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_isomorphic"
PART.Name = "kleiner_isomorphic"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_jar1"
PART.Name = "jar1"
PART.Model = "models/props_lab/jar01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_jar2"
PART.Name = "jar2"
PART.Model = "models/props_lab/jar01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_kennel"
PART.Name = "Rails"
PART.Model = "models/props_lab/kennel.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_keypad"
PART.Name = "keypad"
PART.Model = "models/props_lab/keypad.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_ladder"
PART.Name = "ladder"
PART.Model = "models/props_c17/metalladder002.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_laddertop"
PART.Name = "laddertop"
PART.Model = "models/props_c17/metalladder002b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lamp1"
PART.Name = "lamp1"
PART.Model = "models/props_c17/light_cagelight02_on.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lamp2"
PART.Name = "lamp2"
PART.Model = "models/props_c17/light_cagelight02_on.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lamp3"
PART.Name = "lamp3"
PART.Model = "models/props_c17/light_cagelight02_on.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lamp4"
PART.Name = "lamp4"
PART.Model = "models/props_c17/light_cagelight02_on.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lampdoor"
PART.Name = "kleiner_lampdoor"
PART.Model = "models/props_wasteland/light_spotlight02_lamp.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lampsmalltp"
PART.Name = "lampsmalltp"
PART.Model = "models/props_wasteland/light_spotlight02_lamp.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lampsmalltpbase"
PART.Name = "lampsmalltpbase"
PART.Model = "models/props_wasteland/light_spotlight01_base.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light1"
PART.Name = "light1"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light2"
PART.Name = "light2"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light3"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light4"
PART.Name = "light4"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light5"
PART.Name = "light5"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light6"
PART.Name = "light6"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_light7"
PART.Name = "light7"
PART.Model = "models/props_lab/lab_flourescentlight001b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_locker"
PART.Name = "locker"
PART.Model = "models/props_lab/lockers.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lockerdoora"
PART.Name = "lockerdoora"
PART.Model = "models/props_lab/lockerdoorleft.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lockerdoorb"
PART.Name = "lockerdoorb"
PART.Model = "models/props_lab/lockerdoorright.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_lockerdoorc"
PART.Name = "lockerdoorc"
PART.Model = "models/props_lab/lockerdoorright.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_longflighttoggle"
PART.Name = "kleiner_longflighttoggle"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_magnify"
PART.Name = "magnify"
PART.Model = "models/props_c17/light_magnifyinglamp02.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_manualcoords"
PART.Name = "manualcoords"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_manualflighthitbox"
PART.Name = "manualflight"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_miniteleport"
PART.Name = "mini teleport"
PART.Model = "models/props_lab/miniteleport.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/guy holds cat up to doorbell camera cat proceeds to say mggaow.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_minitppart"
PART.Name = "kleiner_minitppart"
PART.Model = "models/props_lab/miniteleportarc.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_minitppart2"
PART.Name = "kleiner_minitppart2"
PART.Model = "models/props_lab/miniteleportarc.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_monitor"
PART.Name = "Rails"
PART.Model = "models/props_lab/monitor02.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_monitor2"
PART.Name = "monitor2"
PART.Model = "models/props_lab/monitor02.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_physlock"
PART.Name = "kleiner_physlock"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_picture"
PART.Name = "kleiner_picture"
PART.Model = "models/props_lab/frame001a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_pipe1"
PART.Name = "pipe1"
PART.Model = "models/props_lab/pipesystem01b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_pipe2"
PART.Name = "pipe2"
PART.Model = "models/props_lab/pipesystem02b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_pipe3"
PART.Name = "pipe3"
PART.Model = "models/props_lab/pipesystem02d.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_pipe4"
PART.Name = "pipe4"
PART.Model = "models/props_lab/pipesystem02e.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_pipebox"
PART.Name = "pipebox"
PART.Model = "models/props_lab/powerbox02a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_plug"
PART.Name = "plug"
PART.Model = "models/props_lab/tpplug.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_plugs"
PART.Name = "plugs"
PART.Model = "models/props_lab/tpplugholder.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_powerbox1"
PART.Name = "powerbox1"
PART.Model = "models/props_lab/powerbox01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_powerbox2"
PART.Name = "powerbox2"
PART.Model = "models/props_lab/powerbox01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_powerbox3"
PART.Name = "powerbox3"
PART.Model = "models/props_lab/powerbox01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_powerhitbox"
PART.Name = "powerhitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_printer"
PART.Name = "tankconsole"
PART.Model = "models/props_lab/plotter.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_radio"
PART.Name = "radio"
PART.Model = "models/props_lab/citizenradio.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_recievercart1"
PART.Name = "recievercart1"
PART.Model = "models/props_lab/reciever_cart.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_recievercart2"
PART.Name = "recievercart2"
PART.Model = "models/props_lab/reciever_cart.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_redecorate"
PART.Name = "kleiner_redecorate"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_repair"
PART.Name = "kleiner_repair"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_screenstogglehitbox"
PART.Name = "screenstogglehitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_security"
PART.Name = "security"
PART.Model = "models/props_lab/securitybank.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_securityhitbox"
PART.Name = "securityhitbox"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_server"
PART.Name = "server"
PART.Model = "models/props_c17/consolebox03a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_server2"
PART.Name = "server2"
PART.Model = "models/props_lab/reciever01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_server3"
PART.Name = "server3"
PART.Model = "models/props_c17/consolebox03a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_server4"
PART.Name = "server4"
PART.Model = "models/props_lab/reciever01b.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_server5"
PART.Name = "server5"
PART.Model = "models/props_c17/consolebox01a.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)


local PART={}
PART.ID = "kleiner_sonicdispense"
PART.Name = "kleiner_sonicdispense"
PART.Model = "models/FuzzyLeo/Kleiner/buttonhitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_suitcharger"
PART.Name = "suitcharger"
PART.Model = "models/props_combine/suit_charger001.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_tank"
PART.Name = "tank"
PART.Model = "models/props_lab/generator.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_tankconsole"
PART.Name = "tankconsole"
PART.Model = "models/props_lab/generatorconsole.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_tankconsolekeyboard"
PART.Name = "tankconsole"
PART.Model = "models/props_c17/computer01_keyboard.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_tankglass1"
PART.Name = "tank"
PART.Model = "models/props_lab/generatortube.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_tankglass2"
PART.Name = "tank"
PART.Model = "models/props_lab/generatortube.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportbulk"
PART.Name = "teleportbulk"
PART.Model = "models/props_lab/teleportbulk.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleporter"
PART.Name = "teleporter"
PART.Model = "models/props_lab/teleportframe.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportgrate1"
PART.Name = "teleportgrate1"
PART.Model = "models/props_lab/teleportgate.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportgrate2"
PART.Name = "teleportgrate2"
PART.Model = "models/props_lab/teleportgate.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportgrate3"
PART.Name = "teleportgrate3"
PART.Model = "models/props_lab/teleportgate.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportplatform"
PART.Name = "teleportplatform"
PART.Model = "models/props_lab/teleplatform.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportring1"
PART.Name = "teleportring1"
PART.Model = "models/props_lab/teleportring.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportring2"
PART.Name = "teleportring2"
PART.Model = "models/props_lab/teleportring.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportring3"
PART.Name = "teleportring3"
PART.Model = "models/props_lab/teleportring.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportring4"
PART.Name = "teleportring4"
PART.Model = "models/props_lab/teleportring.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportroom"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/Kleiner/teleportroom.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_teleportroom2"
PART.Name = "Rails"
PART.Model = "models/FuzzyLeo/Kleiner/teleportroom2.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_tpswitch"
PART.Name = "tpswitch"
PART.Model = "models/props_lab/tpswitch.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_windowvent"
PART.Name = "windowvent"
PART.Model = "models/props_lab/blastwindow.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)