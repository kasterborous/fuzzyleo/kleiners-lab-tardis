local PART={}
PART.ID = "kleiner_slidingdoor"
PART.Model = "models/FuzzyLeo/Kleiner/slidedoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.5
PART.BypassIsomorphic = true

if SERVER then
	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Use()
        sound.Play("FuzzyLeo/slidingdoor.mp3", self:LocalToWorld(Vector(-247, -15, 56)))
		if ( self:GetOn() ) then
			self:Collide( true )
		else
			self:DontCollide( true )
		end
	end
end


TARDIS:AddPart(PART)

local PART={}
PART.ID = "kleiner_hevdoor"
PART.Model = "models/FuzzyLeo/Kleiner/hevdoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.32
PART.BypassIsomorphic = true

if SERVER then
	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Use()
		sound.Play("FuzzyLeo/garagedoor.mp3", self:LocalToWorld(Vector(319, -136, 72)))
		if ( self:GetOn() ) then
			self:Collide( true )
		else
			self:DontCollide( true )
		end
	end
end

TARDIS:AddPart(PART)