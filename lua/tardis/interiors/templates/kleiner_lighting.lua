TARDIS:AddInteriorTemplate("kleiner_defualtsun", {
	Interior = {
		Suncol = {
			color = Color(255, 255, 255),
		},
		Lamps = {
			sun = {
				color = Color(153, 240, 255),
				texture = "effects/flashlight/square",
				fov = 60,
				distance = 1700,
				brightness = 2,
				pos = Vector(-112.00390625, -583, 296.0009765625),
				ang = Angle(21.994632720947, 90, 0),
				nopower = true,
				shadows = true,
				sprite = false,
			},
			sun2 = {
				color = Color(153, 240, 255),
				texture = "effects/flashlight/square",
				fov = 25,
				distance = 1700,
				brightness = 2,
				pos = Vector(-424.408203125, -463.84576416016, 295.17578125),
				ang = Angle(22, 90, 0),
				nopower = true,
				shadows = true,
				sprite = false,
			},
		},
	},
})

TARDIS:AddInteriorTemplate("kleiner_dusk", {
	Interior = {
		Suncol = {
			color = Color(255, 217, 160),
		},
		Lamps = {
			sun = {
				color = Color(255, 188, 88),
				texture = "effects/flashlight/square",
				fov = 60,
				distance = 1700,
				brightness = 2,
				pos = Vector(-112.00390625, -583, 296.0009765625),
				ang = Angle(21.994632720947, 90, 0),
				nopower = true,
				shadows = true,
				sprite = false,
			},
			sun2 = {
				color = Color(255, 188, 88),
				texture = "effects/flashlight/square",
				fov = 25,
				distance = 1700,
				brightness = 2,
				pos = Vector(-424.408203125, -463.84576416016, 295.17578125),
				ang = Angle(22, 90, 0),
				nopower = true,
				shadows = true,
				sprite = false,
			},
		},
	},
})

TARDIS:AddInteriorTemplate("kleiner_night", {
	Interior = {
		Suncol = {
			color = Color(0, 0, 0),
		},
		Lamps = {
			sun = false,
			sun2 = false,
		},
	},
})

TARDIS:AddInteriorTemplate("kleiner_noon", {
	Interior = {
		Suncol = {
			color = Color(210, 245, 255),
		},
		Lamps = {
			sun = {
				color = Color(255, 227, 184),
				texture = "effects/flashlight/square",
				fov = 60,
				distance = 1700,
				brightness = 2,
				pos = Vector(-112.00390625, -583, 296.0009765625),
				ang = Angle(21.994632720947, 90, 0),
				nopower = true,
				shadows = true,
				sprite = false,
			},
			sun2 = {
				color = Color(255, 227, 184),
				texture = "effects/flashlight/square",
				fov = 25,
				distance = 1700,
				brightness = 2,
				pos = Vector(-424.408203125, -463.84576416016, 295.17578125),
				ang = Angle(22, 90, 0),
				nopower = true,
				shadows = true,
				sprite = false,
			},
		},
	},
})

TARDIS:AddInteriorTemplate("kleiner_cloudy", {
	Interior = {
		Suncol = {
			color = Color(185, 185, 185),
		},
		Lamps = {
			sun = false,
			sun2 = false,
		},
	},
})

