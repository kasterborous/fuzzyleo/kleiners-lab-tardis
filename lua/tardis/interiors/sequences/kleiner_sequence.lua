--Default Interior - Control Sequences (advanced mode)

local Seq = {
    ID = "kleiner_sequence",

    ["kleiner_coordshitbox"] = {
        Controls = {
            "kleiner_physlock",
            "kleiner_float",
            "kleiner_handbrake",
            "kleiner_demathitbox"
        },
        OnFinish = function(self, ply, step, part)
            if IsValid(self) and IsValid(self) then
                self.exterior:Demat()
            end
        end,
        OnFail = function(self, ply, step, part)
            -- Fail stuff
        end,
        Condition = function(self)
            return self.exterior:GetData("vortex",false)==false and self.exterior:GetData("teleport",false)==false
        end
    }
}

TARDIS:AddControlSequence(Seq)